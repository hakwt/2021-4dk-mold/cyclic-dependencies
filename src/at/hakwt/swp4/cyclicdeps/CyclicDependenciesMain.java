package at.hakwt.swp4.cyclicdeps;

public class CyclicDependenciesMain {

    public static void main(String[] args) {
	    Garage fordGarage = new Garage();
        Car ford = new Car(fordGarage);
        Traveler traveler = new Traveler(ford);
        fordGarage.maintainCarFor(traveler);
    }
}
