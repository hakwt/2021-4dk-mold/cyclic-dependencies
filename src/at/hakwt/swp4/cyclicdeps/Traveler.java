package at.hakwt.swp4.cyclicdeps;

public class Traveler {

    private final Car car;

    public Traveler(Car car) {
        this.car = car;
    }

    public void startJourney() {
        this.car.move();
    }

    public Car getCar() {
        return car;
    }
}
